package mx.lania.asynctask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mx.lania.asynctask.dialogs.CountdownDialog;

public class MainActivity extends AppCompatActivity {

    private Button btn_exec;
    private EditText secondsTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
    }

    private void initComponents(){
        //Enlazar elementos del view
        this.btn_exec = (Button) findViewById(R.id.btn_exec);
        this.secondsTxt = (EditText) findViewById(R.id.secondsTxt);

        //Eventos de boton
        this.btn_exec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int time = isValidTime(secondsTxt.getText().toString().trim());
                if( time != -1){
                    CountdownDialog dialogFragment = CountdownDialog.newInstance(time);
                    dialogFragment.show(getSupportFragmentManager(), "dialog");
                }else showMsg("Ingrese un valor entre 10 y 60");
            }
        });
    }

    private int isValidTime(String time){
        int seconds = -1;
        try {
            seconds = Integer.parseInt(time);
        }catch (Exception e){
            return seconds;
        }
        return ((seconds >= 10) && (seconds <= 60)) ? seconds : -1;
    }

    private void showMsg(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}