package mx.lania.asynctask.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import mx.lania.asynctask.R;

public class CountdownDialog extends DialogFragment {

    ProgressDialog progressDialog;

    public static CountdownDialog newInstance(int time){
        CountdownDialog fragment = new CountdownDialog();
        Bundle args = new Bundle();
        args.putInt("time", time);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
                new DialogAsyncTask().execute(getArguments().getInt("time"));
                return progressDialog;
    }

    private class DialogAsyncTask extends AsyncTask<Integer, String, Boolean> {

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setIcon(R.drawable.ic_timer);
            progressDialog.setTitle("Cargando...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setProgress(0);
        }

        @Override
        protected Boolean doInBackground(Integer... params) {
            for (int i = params[0]; i >=0; i--) {
                try {
                    Thread.sleep(1000);
                    this.publishProgress("Por favor, espere " + i + " segundos...");
                } catch (InterruptedException e) {
                    Thread.interrupted();
                    return false;
                }
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result){
                progressDialog.dismiss();
                showMsg("Gracias por esperar..");
            }else
                showMsg("Ups, algo salio mal..");
        }

        @Override
        protected void onProgressUpdate(String... values) {
            progressDialog.setMessage(values[0]);
        }
    }

    private void showMsg(String msg){
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

}